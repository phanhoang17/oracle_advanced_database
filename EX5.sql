-- new sample insert into takes table
select count(*) from takes;
insert into takes(id, course_id, sec_id, year, semester)
values (65901, 313, 1, 2010, 'Fall');

delete from takes where id=65901 and course_id=313 and semester='Fall' and year=2010;
select count(*) from takes;

drop trigger check_no_student_per_classroom;
create or replace trigger check_no_student_per_classroom
-- Can not commit or rollback in a trigger!
-- https://stackoverflow.com/questions/49824417/oracle-pl-sql-how-to-rollback-the-newly-inserted-row
before insert on takes
for each row
declare
    register_count number := 0;
    max_capacity number := 0;
    recent_capacity number := 0;
begin
    select count(*) into register_count
    from takes
    where id=:new.id and course_id=:new.course_id and sec_id=:new.sec_id 
        and year=:new.year and semester=:new.semester;
    
    if register_count > 0 then
        dbms_output.put_line('Sinh vien da dang ki lop');
    else
        select c.capacity into max_capacity
        from section s
        join classroom c
        using (building, room_number)
        where s.course_id=:new.course_id and s.sec_id=:new.sec_id and
                s.semester=:new.semester and s.year=:new.year;
        dbms_output.put_line('max_capacity: ' || max_capacity);
                
        select count(*) into recent_capacity
        from takes t
        where course_id=:new.course_id and sec_id=:new.sec_id and
                semester=:new.semester and year=:new.year;
        dbms_output.put_line('recent_capacity: ' || recent_capacity);
                
        if recent_capacity < max_capacity then
            dbms_output.put_line('Dang ki thanh cong!');
        else   
            raise_application_error(-20000, 'Khong the dang ki lop, vuot qua so luong quy dinh!');
        end if;
    end if;
end;
/

-- test
select count(*) from takes;
delete from takes where id=65901 and course_id=313 and semester='Fall' and year=2010;

insert into takes(id, course_id, sec_id, year, semester)
        values (65901, 313, 1, 2010, 'Fall');
