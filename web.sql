--drop sequence test_seq;
create sequence test_seq;
select test_seq.nextval from dual;

--drop trigger increment_id_student;
create or replace trigger increment_id_student
before insert on student
for each row
declare
    temp_count number := 0;
begin
    loop
        select test_seq.nextval into :new.id from dual;
        
        select count(*) into temp_count from student
        where id=:new.id;
        if temp_count = 0 then
            exit;
        end if;
    end loop;
end;

select count(*) from student;

insert into student (name, dept_name, tot_cred)
values ('Test Name 4', 'Math', 0);

select * from student where lower(name) like '%test%';

update student set name='Test Name 100', dept_name='Physics' where name='Test Name 3';
select * from ddl_info where (lower(name_student) like '%hoang%') or (lower(semester)='fall');