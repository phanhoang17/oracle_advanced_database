-- create a table by using subquery
--create table test_ct as 
--select employee_id, last_name, salary*12 new_salary, hire_date from employees where department_id = 80;
--
--select * from test_ct;
--
--select * from employees where employee_id = 145;

create table dept_ct (
    id number(7) constraint dept_department_id primary key,
    name varchar2(25)
);
desc dept_ct;

insert into dept_ct 
select department_id, department_name from departments;

select count(*) from dept_ct;

create table emp_ct (
    id number(7) constraint emp_aaa primary key,
    last_name varchar2(25),
    first_name varchar2(25),
    dept_id number(7) constraint empdept_fk1 references dept_ct (id)
);

desc emp_ct;

create table employee2_ct as
select employee_id, first_name, last_name, salary, department_id from employees;
desc employee2_ct;
select count(*) from employee2_ct;

drop table employee2_ct;

-- create index
create index emp_last_name_idx on employees (last_name);

-- drop index
drop index emp_last_name_idx;

create index emp_dept_id_idx on emp_ct (dept_id);