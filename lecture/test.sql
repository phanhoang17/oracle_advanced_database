CREATE TABLE HR.persons(
    person_id NUMBER,
    first_name VARCHAR2(50) NOT NULL,
    last_name VARCHAR2(50) NOT NULL,
    PRIMARY KEY(person_id)
);

alter table persons add birthday date not null;

DESCRIBE persons;

alter table persons add (
    phone varchar(20),
    email varchar(100)
)

desc persons;

alter table persons modify birthday date null;

desc persons;

alter table persons modify (
    phone varchar2(20) not null,
    email varchar2(100) not null
);
desc persons;

alter table persons drop column birthday;
desc persons;

alter table persons drop (email, phone); desc persons;

-- rename columns
alter table persons rename column first_name to forename; desc persons;

-- rename table
alter table persons rename to people; desc people;

alter table people rename to people_ct; desc people_ct;

-- create a new table
create table members_ct (
    member_id number,
    first_name varchar2(50),
    last_name varchar2(50),
    primary key (member_id)
);

alter table members_ct add birth_date date not null;
alter table members_ct add (
    created_at timestamp with time zone not null,
    updated_at timestamp with time zone not null
);
desc members_ct;

select * from user_tab_cols;
select count(*) from user_tab_cols where column_name='first_name' and table_name='members_ct';

-- create new table
create table parts_ct (
    part_id number,
    part_name varchar2(255) not null,
    buy_price number(9, 2) check (buy_price > 0),
    primary key (part_id)
);

-- insert values
insert into parts_ct (part_name, buy_price) values ('HDD', 0);
-- throw error here

-- drop table
drop table parts_ct;

create table parts_ct (
    part_id number,
    part_name varchar2(255) not null,
    buy_price number(9, 2) constraint check_positive_buy_price check(buy_price > 0),
    primary key (part_id)
);

insert into parts_ct (part_name, buy_price) values ('screen', -100);
-- throw error here

-- add constraint to a table
alter table parts_ct add cost number (9, 2); desc parts_ct;

-- add multiple constraint to table
alter table parts_ct add constraint check_positive_cost check (cost > 0);
alter table parts_ct add constraint check_valid_cost check (cost > buy_price);

alter table parts_ct rename column cost to cost_;
desc parts_ct;

-- disable constraint
alter table parts_ct disable constraint check_valid_cost;

insert into parts_ct (part_name, buy_price, cost_) values ('laptop', 1000, 1001);