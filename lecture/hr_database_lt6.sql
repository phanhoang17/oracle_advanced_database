select department_id, department_name, location_id, city
from departments
natural join locations;

-- self-join using on clause
select e.last_name emp, m.last_name mgr
from employees e join employees m
on (e.manager_id = m.employee_id);

-- condition
select e.employee_id, e.last_name, e.department_id, d.department_id, d.location_id
from employees e join departments d
on (e.department_id = d.department_id)
and e.manager_id = 149;

-- 3-ways join
select employee_id, city, department_name
from employees e
join departments d
on d.department_id = e.department_id
join locations l
on d.location_id = l.location_id;

-- condition
--select e.last_name, e.salary, j.grade_level
--from employees e join jobs j
--on e.salary
--between j.min_salary and j.max_salary;

-- left outer join
select e.last_name, e.department_id, d.department_id
from employees e left outer join departments d
on (e.department_id = d.department_id);

-- right outer join
select e.last_name, e.department_id, d.department_id
from employees e right outer join departments d
on (e.department_id = d.department_id);

-- full outer join
select e.last_name, d.department_id, d.department_name
from employees e full outer join departments d
on (e.department_id = d.department_id);

-- cross join: cartesian product
select last_name, department_name
from employees
cross join departments;

-- HOMEWORK 1.1
select e.last_name, d.department_name, d.department_id
from employees e
join departments d
on e.department_id = d.department_id;

-- HOMEWORK 1.2
select e1.last_name Employee, e1.employee_id Emp#, e2.last_name Manager, e2.manager_id Mgr#
from employees e1 
join employees e2
on e1.manager_id = e2.employee_id
--where e2.manager_id is not null
order by e1.employee_id;

-- HOMEWORD 1.3
select e1.last_name Employee, e1.hire_date Emp#, e2.last_name Manager, e2.hire_date Mgr#
from employees e1 
join employees e2
on e1.manager_id = e2.employee_id
where e1.hire_date < e2.hire_date
order by e1.employee_id;

select * from employees;
select * from departments;
select * from locations;
select * from jobs;

-- Sub-query
select last_name, job_id, salary
from employees
where job_id = (
    select job_id from employees where employee_id = 141)
and salary > (
    select salary from employees where employee_id = 143);
    
-- NOTE: subquery return no values
select last_name, job_id from employees
where job_id = (select job_id from employees where last_name = 'Haas');
-- empty table

-- multirow select 
select employee_id, last_name, job_id, salary
from employees
where salary < all (
    select salary from employees where job_id = 'IT_PROG')
and job_id <> 'IT_PROG';

select employee_id, last_name, job_id, salary
from employees
where salary < any (
    select salary from employees where job_id = 'IT_PROG')
and job_id <> 'IT_PROG';

-- HOMEWORK 2.1
select employee_id, last_name, salary
from employees 
where salary > (
    select avg(salary) from employees )
and last_name like '%u%';

-- set operation: union
select employee_id, job_id
from employees
union
select employee_id, job_id
from job_history;

-- union all
select employee_id, job_id, department_id
from employees
union all
select employee_id, job_id, department_id
from job_history
order by employee_id;

-- intersect
select employee_id, job_id
from employees
intersect
select employee_id, job_id
from job_history;

-- minus
select employee_id, job_id
from employees
minus
select employee_id, job_id
from job_history;

-- HOMEWORK 3.1
select last_name, department_id, to_char(null) department_name
from employees
union
select to_char(null), department_id, department_name
from departments;

-- HOMEWORK 3.2
select employee_id, job_id
from employees
intersect
select employee_id, job_id
from employees;

-- HOMEWORK 3.3
select country_id, country_name, to_number(null)
from countries
union
select country_id, to_char(null), location_id
from locations
minus 
select to_char(null), to_char(null), location_id
from departments;

select * from departments;
select * from countries;
select * from locations;
select * from employees;
select * from job_history;