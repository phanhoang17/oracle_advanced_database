set serveroutput on
set autoprint on

-- test
select count(*) from section;
--building = 'Chandler' and room_number = 804 and time_slot_id = 'N'
insert into section(course_id, sec_id, year, semester, building, room_number, time_slot_id)
        values (313, 1, 2010, 'Spring', 'Chandler', 804, 'N');
delete from section where course_id=313 and sec_id=1 and semester = 'Spring' and year = 2010 and building = 'Chandler'
    and room_number = 804 and time_slot_id = 'N';

select count(*) from takes;
insert into takes(id, course_id, sec_id, year, semester)
        values (65901, 313, 1, 2010, 'Spring');
delete from takes where id=65901 and course_id=313 and semester='Spring' and year=2010;


drop trigger check_no_student_per_classroom;
create or replace procedure SP_SV_DANG_KI_LOP_HOC (p_student_id number, p_course_id number, p_sec_id number, p_building varchar2, 
    p_room_number number, p_time_slot_id varchar2, p_year number, p_semester varchar2)
as
    temp_count number := 0;
begin
    savepoint before_insert_section;
    select count(*) into temp_count from section;
    dbms_output.PUT_LINE('===> Before insert `section`: ' || temp_count);
    insert into section(course_id, sec_id, year, semester, building, room_number, time_slot_id)
        values (p_course_id, p_sec_id, p_year, p_semester, p_building, p_room_number, p_time_slot_id);
    
    select count(*) into temp_count from takes;
    dbms_output.PUT_LINE('===> Before insert `takes`: ' || temp_count);
    insert into takes(id, course_id, sec_id, year, semester)
        values (p_student_id, p_course_id, p_sec_id, p_year, p_semester);
    
    select count(*) into temp_count from section;  -- insert complete
    dbms_output.PUT_LINE('===> After insert `section`: ' || temp_count);   
    select count(*) into temp_count from takes;  -- insert complete
    dbms_output.PUT_LINE('===> After insert `takes`: ' || temp_count);
    raise_application_error( -20001, 'THIS IS A CUSTOM ERROR FOR TESTING' );
    exception
        when others then
            dbms_output.PUT_LINE('===> SP_SV_DANG_KI_LOP_HOC ERROR: ' || SQLERRM);
            rollback to before_insert_section;
            dbms_output.PUT_LINE('===> ROLLBACK COMPLETE!!!');            
            select count(*) into temp_count from section;
            dbms_output.PUT_LINE('===> After rollback `section`: ' || temp_count);
            select count(*) into temp_count from takes;
            dbms_output.PUT_LINE('===> After rollback `takes`: ' || temp_count);
end;
/

exec SP_SV_DANG_KI_LOP_HOC(65901, 313, 1, 'Chandler', 804, 'N', 2010, 'Spring');


delete from student where id=34331;
select * from ddl_info where id_student=65901;

insert into course (course_id, title, dept_name, credits)
values (999, 'Test Course', 'Math', 12);

select * from course where course_id=1000;

select id, name, dept_name from student where id=34331;

update course 
set dept_name='Physics', title='aaa'
where course_id=999;
-- del: xoa student
-- update: cap nhat student
-- insert: them student